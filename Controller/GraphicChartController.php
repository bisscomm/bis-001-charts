<?php
namespace Bci\ChartsBundle\Controller;

use Bci\ChartsBundle\Entity\Definition;
use Bci\ChartsBundle\Entity\Element;
use Bci\ChartsBundle\Entity\Property;
use Bci\ChartsBundle\Entity\Selector;
use Bci\ChartsBundle\Form\DefinitionType;
use Bci\ChartsBundle\Form\SelectorType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;

class GraphicChartController
    extends Controller
{
    /**
     * @Config\Route("/", name="bci_chart")
     *
     * @Config\Template
     * @return array
     */
    public function index()
    {
        return [
            'title' => 'CMS GRAPHIC CHART',
            'bundles' => [
                'bundle 1', 'bundle 2', 'bundle 3'
            ]
        ];
    }
}
