$("a > .fa-plus-circle").click(function () {
    if ($(this).parent().is(".collapsed")) {
        $(this).addClass("fa-minus-circle");
        $(this).removeClass("fa-plus-circle");
    } else {
        $(this).addClass("fa-plus-circle");
        $(this).removeClass("fa-minus-circle");
    }
});

$("a:has('.fa-chevron-right')").click(function () {
    if ($(this).is(".collapsed")) {
        $(this).find('i').addClass("fa-chevron-down");
        $(this).find('i').removeClass("fa-chevron-right");
    } else {
        $(this).find('i').addClass("fa-chevron-right");
        $(this).find('i').removeClass("fa-chevron-down");
    }
});

