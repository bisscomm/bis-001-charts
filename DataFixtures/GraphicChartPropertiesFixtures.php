<?php

namespace Bci\ChartsBundle\DataFixtures;

use Bci\ChartsBundle\Entity\Property;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class GraphicChartPropertiesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        foreach ($this->getProperties() as $data) {
            $property = new Property();
            $property
                ->setName($data['name'])
                ->setAvailValues($data['availValues'])
                ->setDefaultValue($data['defaultValue'])
                ->setConfig($data['config']);
            $manager->persist($property);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            [
                'name'          => 'font-style',
                'availValues'   => [
                    'normal', 'italic', 'oblique',
                    'initial', 'inherit'
                ],
                'defaultValue' => '',
                'config'        => []
            ],
            [
                'name'          => 'font-family',
                'availValues'   => ['Arial, sans-serif'],
                'defaultValue' => '',
                'config'        => []
            ],
            [
                'name'          => 'font-size',
                'availValues'   => [],
                'defaultValue' => '',
                'config'        => [
                    'suffixes'  => ['px', 'em']
                ]
            ],
            [
                'name'          => 'font-weight',
                'availValues'   => [
                    'normal', 'bold', 'bolder', 'lighter',
                    100, 200, 300, 400, 500, 600, 700, 800, 900,
                    'initial', 'inherit'
                ],
                'defaultValue' => '',
                'config'        => []
            ],
            [
                'name'          => 'letter-spacing',
                'availValues'   => [],
                'defaultValue' => '',
                'config'        => [
                    'suffixes'  => ['px', 'em']
                ]
            ],
            [
                'name'          => 'text-transform',
                'availValues'   => [
                    'none', 'capitalize', 'uppercase', 'lowercase',
                    'initial', 'inherit'
                ],
                'defaultValue' => '',
                'config'        => []
            ],
            [
                'name'          => 'color',
                'availValues'   => [],
                'defaultValue' => '',
                'config'        => [
                    'patterns'  => [
                        'rgb'  => 'rgb($r, $g, $b)',
                        'rgba' => 'rgba($r, $g, $b, $a)',
                        'hex'  => '#$color'
                    ]
                ]
            ],
        ];
    }
}