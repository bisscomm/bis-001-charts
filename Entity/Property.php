<?php

namespace Bci\ChartsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Bci\ChartsBundle\\Repository\PropertyRepository")
 * @ORM\Table(name="graphic_chart_properties")
 */
class Property
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=25)
     */
    protected $name;

    /**
     * @var array
     * @ORM\Column(type="array")
     */
    protected $config;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $defaultValue;

    /**
     * @var array
     * @ORM\Column(type="array")
     */
    protected $availValues;



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Property
     */
    public function setName(string $name): Property
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @param array $config
     * @return Property
     */
    public function setConfig(array $config): Property
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultValue(): string
    {
        return $this->defaultValue;
    }

    /**
     * @param string $defaultValue
     * @return Property
     */
    public function setDefaultValue(string $defaultValue): Property
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @return array
     */
    public function getAvailValues(): array
    {
        return $this->availValues;
    }

    /**
     * @param array $availValues
     * @return Property
     */
    public function setAvailValues(array $availValues): Property
    {
        $this->availValues = $availValues;
        return $this;
    }
}