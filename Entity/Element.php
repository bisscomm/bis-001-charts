<?php
namespace Bci\ChartsBundle\Entity;

class Element
{
    protected $fontStyle;
    protected $fontFamily;
    protected $fontSize;
    protected $fontWeight;
    protected $lineHeight;
    protected $letterSpacing;
    protected $textTransform;
    protected $color;

    protected $fontStyleMobile;
    protected $fontFamilyMobile;
    protected $fontSizeMobile;
    protected $fontWeightMobile;
    protected $lineHeightMobile;
    protected $letterSpacingMobile;
    protected $textTransformMobile;
    protected $colorMobile;







    

    /**
     * @return mixed
     */
    public function getFontStyle()
    {
        return $this->fontStyle;
    }

    /**
     * @param mixed $fontStyle
     *
     * @return self
     */
    public function setFontStyle($fontStyle)
    {
        $this->fontStyle = $fontStyle;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFontFamily()
    {
        return $this->fontFamily;
    }

    /**
     * @param mixed $fontFamily
     *
     * @return self
     */
    public function setFontFamily($fontFamily)
    {
        $this->fontFamily = $fontFamily;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFontSize()
    {
        return $this->fontSize;
    }

    /**
     * @param mixed $fontSize
     *
     * @return self
     */
    public function setFontSize($fontSize)
    {
        $this->fontSize = $fontSize;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFontWeight()
    {
        return $this->fontWeight;
    }

    /**
     * @param mixed $fontWeight
     *
     * @return self
     */
    public function setFontWeight($fontWeight)
    {
        $this->fontWeight = $fontWeight;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLineHeight()
    {
        return $this->lineHeight;
    }

    /**
     * @param mixed $lineHeight
     *
     * @return self
     */
    public function setLineHeight($lineHeight)
    {
        $this->lineHeight = $lineHeight;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLetterSpacing()
    {
        return $this->letterSpacing;
    }

    /**
     * @param mixed $letterSpacing
     *
     * @return self
     */
    public function setLetterSpacing($letterSpacing)
    {
        $this->letterSpacing = $letterSpacing;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTextTransform()
    {
        return $this->textTransform;
    }

    /**
     * @param mixed $textTransform
     *
     * @return self
     */
    public function setTextTransform($textTransform)
    {
        $this->textTransform = $textTransform;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     *
     * @return self
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFontStyleMobile()
    {
        return $this->fontStyleMobile;
    }

    /**
     * @param mixed $fontStyleMobile
     *
     * @return self
     */
    public function setFontStyleMobile($fontStyleMobile)
    {
        $this->fontStyleMobile = $fontStyleMobile;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFontFamilyMobile()
    {
        return $this->fontFamilyMobile;
    }

    /**
     * @param mixed $fontFamilyMobile
     *
     * @return self
     */
    public function setFontFamilyMobile($fontFamilyMobile)
    {
        $this->fontFamilyMobile = $fontFamilyMobile;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFontSizeMobile()
    {
        return $this->fontSizeMobile;
    }

    /**
     * @param mixed $fontSizeMobile
     *
     * @return self
     */
    public function setFontSizeMobile($fontSizeMobile)
    {
        $this->fontSizeMobile = $fontSizeMobile;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFontWeightMobile()
    {
        return $this->fontWeightMobile;
    }

    /**
     * @param mixed $fontWeightMobile
     *
     * @return self
     */
    public function setFontWeightMobile($fontWeightMobile)
    {
        $this->fontWeightMobile = $fontWeightMobile;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLineHeightMobile()
    {
        return $this->lineHeightMobile;
    }

    /**
     * @param mixed $lineHeightMobile
     *
     * @return self
     */
    public function setLineHeightMobile($lineHeightMobile)
    {
        $this->lineHeightMobile = $lineHeightMobile;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLetterSpacingMobile()
    {
        return $this->letterSpacingMobile;
    }

    /**
     * @param mixed $letterSpacingMobile
     *
     * @return self
     */
    public function setLetterSpacingMobile($letterSpacingMobile)
    {
        $this->letterSpacingMobile = $letterSpacingMobile;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTextTransformMobile()
    {
        return $this->textTransformMobile;
    }

    /**
     * @param mixed $textTransformMobile
     *
     * @return self
     */
    public function setTextTransformMobile($textTransformMobile)
    {
        $this->textTransformMobile = $textTransformMobile;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getColorMobile()
    {
        return $this->colorMobile;
    }

    /**
     * @param mixed $colorMobile
     *
     * @return self
     */
    public function setColorMobile($colorMobile)
    {
        $this->colorMobile = $colorMobile;

        return $this;
    }
}