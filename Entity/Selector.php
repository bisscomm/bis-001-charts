<?php

namespace Bci\ChartsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Bci\ChartsBundle\Repository\ElementRepository")
 * @ORM\Table(name="graphic_chart_selectors")
 */
class Selector
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Definition", mappedBy="selector")
     */
    protected $definitions;

    /**
     * Selector constructor.
     */
    public function __construct()
    {
        $this->setDefinitions(new ArrayCollection());
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Selector
     */
    public function setName(string $name): Selector
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getDefinitions(): ArrayCollection
    {
        return $this->definitions;
    }

    /**
     * @param ArrayCollection $definitions
     * @return Selector
     */
    public function setDefinitions(ArrayCollection $definitions): Selector
    {
        $this->definitions = $definitions;
        return $this;
    }

    /**
     * @param Definition $definition
     * @return Selector
     */
    public function addDefinition(Definition $definition): Selector
    {
        if (!$this->hasDefinition($definition)) {
            $definition->setSelector($this);
            $this->getDefinitions()->add($definition);
        }
        return $this;
    }

    /**
     * @param Definition $definition
     * @return bool
     */
    public function hasDefinition(Definition $definition): bool
    {
        return $this->getDefinitions()->contains($definition);
    }

    /**
     * @param Definition $definition
     * @return Selector
     */
    public function removeDefinition(Definition $definition): Selector
    {
        if ($this->hasDefinition($definition)) {
            $this->getDefinitions()->removeElement($definition);
        }
        return $this;
    }
}