<?php

namespace Bci\ChartsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Bci\ChartsBundle\Repository\PropertyRepository")
 * @ORM\Table(name="graphic_chart_definitions")
 */
class Definition
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Selector
     * @ORM\ManyToOne(targetEntity="Selector", inversedBy="definitions")
     * @ORM\JoinColumn(name="selector_id", referencedColumnName="id")
     */
    protected $selector;

    /**
     * @var Property
     * @ORM\ManyToOne(targetEntity="Property")
     * @ORM\JoinColumn(name="property_id", referencedColumnName="id")
     */
    protected $property;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $value;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $media;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Definition
     */
    public function setId(int $id): Definition
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getSelector(): Selector
    {
        return $this->selector;
    }

    /**
     * @param Selector $selector
     * @return Definition
     */
    public function setSelector(Selector $selector): Definition
    {
        $this->selector = $selector;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param Property $property
     * @return Definition
     */
    public function setProperty(Property $property): Definition
    {
        $this->property = $property;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value|'';
    }

    /**
     * @param string $value
     * @return Property
     */
    public function setValue(string $value): Definition
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getMedia(): string
    {
        return $this->media|'';
    }

    /**
     * @param string $media
     * @return Property
     */
    public function setMedia(string $media): Definition
    {
        $this->media = $media;
        return $this;
    }
}